import React from 'react';
import './Message.css';

const Message = (props) => {

	const onReply = (e) => {
		e.preventDefault();
		console.log(e.target);
	}
    return (
        <div className="message" id={props.message.id}>
					<div>
						<p className="message-text">{props.message.text}</p>
						<p className="likes">{props.message.likes}Likes</p>
						<p className="dislikes">{props.message.dislikes}Dislikes</p>
					</div>
					<div>
						<span className="message-user-name">{props.message.user}</span>
						<button className="reply-button" onClick={onReply}> Reply </button>
						</div>
        </div>
    )
}
export default Message;