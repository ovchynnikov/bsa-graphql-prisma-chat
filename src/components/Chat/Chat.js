import React from 'react';
import Message from '../Message/Message';
import { MESSAGES_QUERY, NEW_MESSAGES_SUBSCRIPTION, NEW_REPLY_SUBSCRIPTION } from '../../queries'
import { Query } from 'react-apollo';

const Chat = () => {

	const _subscribeToNewMessages = subscribeToMore => {
		subscribeToMore({
			document: NEW_MESSAGES_SUBSCRIPTION,
			updateQuery: (prev, { subscriptionData }) => {
				if (!subscriptionData.data) {
					return prev;
				}

				const newMessage = subscriptionData.data.newMessage;
				newMessage.likes = 0;
				newMessage.dislikes = 0;
				const exists = prev.messages.find(({ id }) => id === newMessage.id);
				if (exists) return prev;
				return {
						messages: [...prev.messages, newMessage],
				};
			},
		})
	}


	const _subscribeToNewReplies = subscribeToMore => {
		subscribeToMore({
			document: NEW_REPLY_SUBSCRIPTION,
			updateQuery: (prev, { subscriptionData }) => {
				console.log('!subscriptionData');

				if (!subscriptionData.data) {
					return prev;
				}
				console.log('subscriptionData',subscriptionData);
				const messages = prev.messages;
				const newReply = subscriptionData.data.newReply;

				const messageIndexToUpdate = messages.findIndex(
					(message) => message.id === newReply.message.id
				);

				const messageToUpdate = messages[messageIndexToUpdate];

				const updatedMessage = {
					...messageToUpdate,
					replies: [...(messageToUpdate.replies), newReply],
				};

				return {
					messages: [
						...prev.messages,
						messages.slice(0, messageIndexToUpdate)
							.concat(
								updatedMessage,
								messages.slice(messageIndexToUpdate + 1)
							),
					],
				};
			},
		})
	}


    return (
			<Query query={MESSAGES_QUERY}>
				{({ loading, error, data, subscribeToMore }) => {
					if (loading) return <div>Loading...</div>;
					if (error) return <div>Fetch Error</div>;
					_subscribeToNewMessages(subscribeToMore);
					_subscribeToNewReplies(subscribeToMore)

					const { messages } = data;

				 return (
				 	
				 	 <div className="message-list">
				 	   <h2>Chat</h2>
				 	   { messages.map( (message) => <Message key={message.id} message={message} />) }
           </div>
				 )
				}}
			</Query>
  )
}

export default Chat;