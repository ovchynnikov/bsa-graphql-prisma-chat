function message(parent, args, context) {
    return context.prisma.replies({
        id: parent.id,
    }).message();
}

module.exports = {
    message
}