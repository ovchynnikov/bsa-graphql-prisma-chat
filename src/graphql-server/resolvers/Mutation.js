function postMessage(parent, args, context, info) {
    return context.prisma.createMessage({
        text: args.text,
        userId: args.userId,
        likes: 0,
        dislikes: 0
    });
}

async function postReply(parent, args, context, info) {
    const messageExists = await context.prisma.$exists.message({
        id: args.messageId
    });
    if (!messageExists) {
        throw new Error(`Message with ID ${args.messageId} doesn't exist`)
    }
    return context.prisma.createReply({
        text: args.text,
        message: { connect: { id: args.messageId } },
    });
}

module.exports = {
    postMessage,
    postReply
}