async function messages(parent, args, context) {
    const messages = await context.prisma.messages();

    return messages;
}

module.exports = {
    messages
}