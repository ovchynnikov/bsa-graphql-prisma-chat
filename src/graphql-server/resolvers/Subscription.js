const newMessageSubscribe = (parent, args, context, info) => context.prisma.$subscribe.message({ mutation_in: ['CREATED'] }).node();

const newMessage = {
    subscribe: newMessageSubscribe,
    resolve: payload => payload
};

const newReplySubscribe = (parent, args, context, info) => context.prisma.$subscribe.reply({ mutation_in: ['CREATED'] }).node();

const newReply = {
    subscribe: newReplySubscribe,
    resolve: payload => payload
};


module.exports = {
    newMessage,
    newReply
};