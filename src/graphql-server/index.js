const { GraphQLServer } = require('graphql-yoga');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const Subscription = require('./resolvers/Subscription');
const { prisma } = require('./prisma/..server/generated/prisma-client');

const resolvers = {
    Query,
    Mutation,
    Subscription,
    Message: {
        ...require('./resolvers/Message'),
    },
    Reply: {
        ...require('./resolvers/Reply'),
    },
}

const server = new GraphQLServer({
    typeDefs: './schema.graphql',
    resolvers,
    context: { prisma }
});

server.start(() => console.log('http://localhost:4000'));