import gql from 'graphql-tag';

export const MESSAGES_QUERY = gql `
	query messageQuery {
		messages {
    id
    text
    likes
    dislikes
    replies {
      id
      text
    }
  }
	}
`;

export const NEW_MESSAGES_SUBSCRIPTION = gql `
	subscription {
		newMessage {
			id
			text
			replies {
				id
				text
			}
		}
	}
	`;

export const NEW_REPLY_SUBSCRIPTION = gql `
  subscription {
    newReply {
      id
      text
      message {
        id
      }
    }
  }
`;